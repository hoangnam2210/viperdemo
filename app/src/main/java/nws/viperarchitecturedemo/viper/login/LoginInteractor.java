package nws.viperarchitecturedemo.viper.login;

import nws.viperarchitecturedemo.entities.UserEntity;

class LoginInteractor implements LoginContracts.Interactor {

    private LoginContracts.InteractorOutput output;

    LoginInteractor(LoginContracts.InteractorOutput output) {
        this.output = output;
    }

    @Override
    public void unregister() {
        output = null;
    }

    @Override
    public void login(String username, String password) {
        if (username.equals("nws") && password.equals("12345")) {
            if (output != null) output.onLoginSuccess(new UserEntity(username, password));
        } else {
            if (output != null) output.onLoginError("Login Fail");
        }
    }
}
