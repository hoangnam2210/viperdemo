package nws.viperarchitecturedemo.viper.login;

import android.app.Activity;
import android.content.Intent;

import nws.viperarchitecturedemo.entities.UserEntity;
import nws.viperarchitecturedemo.viper.home.HomeActivity;

class LoginRouter implements LoginContracts.Router {

    private Activity activity;

    LoginRouter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void unregister() {
        activity = null;
    }

    @Override
    public void presentHomeScreen(UserEntity user) {
        if (activity != null) {
            activity.startActivity(new Intent(activity, HomeActivity.class));
        }
    }
}
