package nws.viperarchitecturedemo.viper.login;

import android.app.Activity;

import nws.viperarchitecturedemo.entities.UserEntity;

class LoginPresenter implements LoginContracts.Presenter, LoginContracts.InteractorOutput {

    private LoginContracts.View view;
    private LoginContracts.Interactor interactor = new LoginInteractor(this);
    private LoginContracts.Router router;

    LoginPresenter(LoginContracts.View view) {
        this.view = view;
        router = new LoginRouter((Activity) view);
    }

    @Override
    public void onDestroy() {
        view = null;
        interactor.unregister();
        interactor = null;
        router.unregister();
        router = null;
    }

    @Override
    public void onLoginButtonPressed(String username, String password) {
        interactor.login(username, password);
    }

    @Override
    public void onLoginSuccess(UserEntity user) {
        if (router != null) router.presentHomeScreen(user);
    }

    @Override
    public void onLoginError(String message) {
        view.showError(message);
    }
}
