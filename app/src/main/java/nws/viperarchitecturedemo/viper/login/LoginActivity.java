package nws.viperarchitecturedemo.viper.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import nws.viperarchitecturedemo.R;
import nws.viperarchitecturedemo.bases.BaseActivity;

public class LoginActivity extends BaseActivity implements LoginContracts.View {

    private LoginContracts.Presenter presenter = null;

    private EditText etUsername, etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this);
        initView();
    }

    @Override
    protected void onDestroy() {
        if (presenter != null) presenter.onDestroy();
        presenter = null;
        super.onDestroy();
    }

    private void initView(){
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (presenter != null) presenter.onLoginButtonPressed(etUsername.getText().toString(),
                        etPassword.getText().toString());
            }
        });
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
