package nws.viperarchitecturedemo.viper.login;

import nws.viperarchitecturedemo.entities.UserEntity;

interface LoginContracts {

    interface View {
        void showError(String message);
    }

    interface Presenter {
        void onDestroy();

        void onLoginButtonPressed(String username, String password);
    }

    interface Interactor {
        void unregister();

        void login(String username, String password);
    }

    interface InteractorOutput {
        void onLoginSuccess(UserEntity user);

        void onLoginError(String message);
    }

    interface Router {
        void unregister();

        void presentHomeScreen(UserEntity user);
    }
}
