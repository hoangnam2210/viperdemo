package nws.viperarchitecturedemo.viper.home;

import android.app.Activity;
import android.os.Bundle;

import nws.viperarchitecturedemo.R;

public class HomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
